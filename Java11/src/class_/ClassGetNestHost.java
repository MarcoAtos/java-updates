package class_;

/**
 * Voorbeeld van de nieuwe Java 11 Class getNestHost() Methode
 *
 * Deze methode geeft de outer class terug van de class waarop de methode aangevraagd wordt.
 */
public class ClassGetNestHost {

    private class InnerClass {}

    private interface InnerInterface {}

    public static void main(final String[] args) {
        System.out.println(ClassGetNestHost.class.getNestHost());
        System.out.println(InnerClass.class.getNestHost());
        System.out.println(InnerInterface.class.getNestHost());
        System.out.println(ExtendedClass.class.getNestHost());
    }
}
