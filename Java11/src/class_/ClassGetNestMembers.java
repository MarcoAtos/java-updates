package class_;

import java.util.Arrays;

/**
 * Voorbeeld van de nieuwe Java 11 getNestMembers() methode.
 *
 * Deze methode geeft alle member classes terug van de class
 */
public class ClassGetNestMembers {

    private class NestedClassA {
    }

    private class NestedClassB {
    }

    public static void main(final String[] args) {
        Arrays.stream(ClassGetNestMembers.class.getNestMembers()).forEach(System.out::println);
        Arrays.stream(ClassGetNestMembers.NestedClassA.class.getNestMembers()).forEach(System.out::println);
    }
}
