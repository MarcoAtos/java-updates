package class_;

/**
 * Voorbeeld van de nieuwe Java 11 isNestMateOf() methode
 *
 * Deze methode geeft aan of de class een outer of inner class is van de gebruikte class.
 */
public class ClassIsNestMateOf {

    private class NestedClassA {
    }

    private class NestedClassB {
    }

    public static void main(final String[] args) {
        final Class<?> nestedClassA = NestedClassA.class;
        final Class<?> nestedClassB = NestedClassB.class;
        final Class<?> classIsNestMateOf = ClassIsNestMateOf.class;
        final Class<?> classGetNestHost = ClassGetNestHost.class;
        final Class<?> extendedClass = ExtendedClass.class;

        System.out.printf("'%s' isNestMateOf '%s' = %s%n", classIsNestMateOf.getSimpleName(),
                          nestedClassA.getSimpleName(), classIsNestMateOf.isNestmateOf(nestedClassA));
        System.out.printf("'%s' isNestMateOf '%s' = %s%n", classIsNestMateOf.getSimpleName(),
                          nestedClassB.getSimpleName(), classIsNestMateOf.isNestmateOf(nestedClassB));
        System.out.printf("'%s' isNestMateOf '%s' = %s%n", nestedClassA.getSimpleName(),
                          nestedClassB.getSimpleName(), nestedClassA.isNestmateOf(nestedClassB));
        System.out.printf("'%s' isNestMateOf '%s' = %s%n", classIsNestMateOf.getSimpleName(),
                          classGetNestHost.getSimpleName(), classIsNestMateOf.isNestmateOf(classGetNestHost));
        System.out.printf("'%s' isNestMateOf '%s' = %s%n", extendedClass.getSimpleName(),
                          classGetNestHost.getSimpleName(), extendedClass.isNestmateOf(classGetNestHost));
    }
}
