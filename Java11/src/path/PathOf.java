package path;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Voorbeeld van de nieuwe Java 11 'Path of' methoden met Strings en URI
 */
public class PathOf {

    public static void main(final String[] args) {
        final Path path = Path.of("/home", "m", "IntellijProjects", "java-updates", "Java11", "src", "path");
        System.out.println(Files.exists(path));
        System.out.println(path);

        final URI uri = URI.create("file:///home/m/IntellijProjects/java-updates/Java11/src/path/textfile.txt");
        System.out.println(uri);
        final Path pathVanUri = Path.of(uri);
        System.out.println(pathVanUri);
        System.out.println(Files.exists(pathVanUri));
    }
}
