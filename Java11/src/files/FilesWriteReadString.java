package files;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Voorbeeld van de nieuwe Java 11 Files writeString() en readString() methoden
 */
public class FilesWriteReadString {

    public static void main(final String[] args) throws IOException {
        final Path path = Path.of("/home", "m", "IntellijProjects", "java-updates", "Java11", "src", "files", "textfile.txt");
        Files.writeString(path, "Nieuw toegevoegde regel");
        System.out.println(Files.readString(path));

        final Charset latinCharset = Charset.forName("ISO-8859-3");
        Files.writeString(path, "Nog een regel", latinCharset);
        System.out.println(Files.readString(path, latinCharset));
    }
}
