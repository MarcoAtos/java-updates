package httpclient;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

/**
 * Voorbeeld om te beginnen met de nieuwe Java 11 http client
 *
 * Java 9 had al een http client, maar beschikbaar als incubating API in de package jdk.incubator.http
 *
 * Support voor zowel HTTP 1.1 en HTTP 2
 */
public class HttpClientAPI {

    public static void main(final String[] args) {
        //Bouwen van request
        final HttpRequest request = HttpRequest.newBuilder()
                                         .uri(URI.create("http://www.example.com/"))
                                         .GET()
                                         .build();
        //Aanmaken response body handler
        final HttpResponse.BodyHandler<String> bodyHandler = HttpResponse.BodyHandlers.ofString();

        //Zenden request en ontvangen response via HttpClient
        final HttpClient client = HttpClient.newHttpClient();
        final CompletableFuture<HttpResponse<String>> future = client.sendAsync(request, bodyHandler);
        future.thenApply(HttpResponse::body)
              .thenAccept(System.out::println)
              .join();
    }
}
