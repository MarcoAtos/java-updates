package io;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

/**
 * Voorbeeld van de nieuwe Java 11 IO nullWriter() methode
 */
public class IoNullWriter {

    public static void main(final String[] args) throws IOException {
        final Writer writer1 = new StringWriter();
        final Writer writer2 = Writer.nullWriter();
        final Writer writer3 = new CharArrayWriter();
        final List<Writer> writers = List.of(writer1, writer2, writer3);

        for (final Writer writer : writers) {
            writer.write("foo bar");
            writer.close();
        }

        writers.forEach(writer -> {
            System.out.printf("Writer type: %s, toString: %s%n", writer.getClass().getSimpleName(), writer.toString());
        });
    }
}
