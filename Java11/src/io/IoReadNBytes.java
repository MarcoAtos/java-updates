package io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Voorbeeld van de nieuwe Java 11 IO readNBytes methode
 */
public class IoReadNBytes {
    
    public static void main(final String[] args) throws IOException {
        final InputStream inputStream = new ByteArrayInputStream("foo bar".getBytes());
        final byte[] bytes = inputStream.readNBytes(3);

        System.out.println(new String(bytes));
        inputStream.close();
    }
}
