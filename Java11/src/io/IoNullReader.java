package io;

import java.io.BufferedReader;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

/**
 * Voorbeeld van de ni euwe Java 11 IO nullReader() methode
 */
public class IoNullReader {

    public static void main(final String[] args) throws IOException {
        final Reader reader1 = new StringReader("foo bar");
        final Reader reader2 = Reader.nullReader();
        final Reader reader3 = new CharArrayReader(new char[] {'a', 'b', 'c'});

        List.of(reader1, reader2, reader3).stream()
            .map(BufferedReader::new)
            .map(BufferedReader::lines)
            .forEach(stream -> stream.forEach(System.out::println));

        reader1.close();
        reader2.close();
        reader3.close();
    }
}
