package io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Voorbeeld van de nieuwe Java 11 io FileReader en FileWriter constructors met charset
 */
public class IoFileReaderWriter {

    public static void main(final String[] args) throws IOException {
        final Path path = Files.createTempFile("test", ".txt");
        final File targetFile = path.toFile();
        targetFile.deleteOnExit();

        final Charset latinCharset = Charset.forName("ISO-8859-3");
        //FileWriter nieuwe constructor
        final FileWriter fw = new FileWriter(targetFile, latinCharset);
        fw.write("test filum");
        fw.close();;
        //FileReader nieuwe constructor
        final FileReader fr = new FileReader(targetFile, latinCharset);
        new BufferedReader(fr).lines().forEach(System.out::println);
        fr.close();
    }
}
