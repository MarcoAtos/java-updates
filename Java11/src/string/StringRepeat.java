package string;

/**
 * Voorbeeld van de nieuwe Java 11 repeat() methode
 *
 * Voordelen:
 * - Sneller
 * - Geen imports nodig
 * - Veiliger
 */
public class StringRepeat {

    public static void main(final String[] args) {
        final String string = "Test ";

        final String oudeManier = new String(new char[3]).replace("\0", string);
        System.out.println("Oude manier: " + oudeManier);

        final String resultaatRepeat0 = string.repeat(0);
        System.out.println(resultaatRepeat0);

        final String resultaatRepeat1 = string.repeat(1);
        System.out.println(resultaatRepeat1);

        final String resultaatRepeat2 = string.repeat(2);
        System.out.println(resultaatRepeat2);

        final String resultaatRepeat3 = string.repeat(3);
        System.out.println(resultaatRepeat3);

        final String resultaatRepeat4 = string.repeat(4);
        System.out.println(resultaatRepeat4);
    }
}
