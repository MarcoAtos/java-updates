package string;

/**
 * Voorbeeld van de nieuwe Java 11 Lines() methode
 */
public class StringLines {

    public static void main(final String[] args) {
        final String tekstMetMeerdereRegels = "foo\nbar\nfoo\nbar";
        tekstMetMeerdereRegels.lines().forEach(System.out::println);
    }
}
