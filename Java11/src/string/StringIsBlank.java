package string;

/**
 * Voorbeeld van de nieuwe Java 11 String isBlank() methode
 */
public class StringIsBlank {

    public static void main(final String[] args) {

        final boolean oudeManier = " ".trim().length() == 0;
        System.out.println(oudeManier);

        final boolean resultaatIsBlank = " ".isBlank();
        System.out.println(resultaatIsBlank);

        // Pas op! In tegenstelling tot de StringUtils, geeft het controleren van een null string een NullPointerException!
        final String nullString = null;
        nullString.isBlank();
    }
}
