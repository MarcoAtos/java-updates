package string;

/**
 * Voorbeeld van de nieuwe Java 11 Strip methode
 *
 * Verschil met trim() methode:
 * - Unicode aware
 * - strip() is tot 5x sneller dan trim() bij lege strings (alloceren geheugen in gebruikte StringLatin1 methoden)
 */
public class StringStrip {

    public static void main(final String[] args) {
        final String teStrippen = "    test    ";

        //Verschil tyssen trim en strip, waarbij \u2005 een ASCII code is voor een whitespace.
        System.out.println("\u2005  TestTrim".trim());
        System.out.println("\u2005  TestStrip".strip());
        
        final String resultaatStrip = teStrippen.strip();
        System.out.println(resultaatStrip);
        final String resultaatLeadingStrip = teStrippen.stripLeading();
        System.out.println(resultaatLeadingStrip);
        final String resultaatTrailingStrip = teStrippen.stripTrailing();
        System.out.println(resultaatTrailingStrip);
    }
}
