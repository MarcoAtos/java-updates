package lambda;

import java.util.stream.IntStream;

/**
 * Voorbeeld van de nieuwe Java 11 Lambda mogelijkheden met de var parameters
 */
public class LambdaVar {

    public static void main(final String[] args) {
        System.out.println("Lambda 1");
        IntStream.of(1, 2, 3, 5, 6, 7, 8, 9)
                 .filter(i -> i % 3 == 0)
                 .forEach(System.out::println);

        System.out.println("Lambda 2");
        IntStream.of(1, 2, 3, 5, 6, 7, 8, 9)
                 .filter((var i) -> i % 3 == 0)
                 .forEach(System.out::println);

        System.out.println("Lambda 3");
        IntStream.of(1, 2, 3, 5, 6, 7, 8, 9)
                 .filter((final var i) -> i % 3 == 0)
                 .forEach(System.out::println);

        System.out.println("Lambda 4");
        IntStream.of(1, 2, 3, 5, 6, 7, 8, 9)
                 .filter((@Nonnull final var i) -> i % 3 == 0)
                 .forEach(System.out::println);
    }
}
