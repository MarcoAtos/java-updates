package collection;

import java.util.Arrays;
import java.util.List;

/**
 * Voorbeeld van de nieuwe Java 11 toArray() methode
 *
 * Deze methode is nu toegevoegd aan de Collection interface en kan daarmee nu ook een nieuwe array teruggeven op basis van de input.
 */
public class CollectionNaarArray {

    public static void main(final String[] args) {
        final List<String> list = List.of("Appel", "Banaan", "Kiwi");

        //Oude methoden (List)
        final String[] array = list.toArray(new String[0]);
        System.out.println(Arrays.toString(array));
        final Object[] objects = list.toArray();
        System.out.println(Arrays.toString(objects));

        //Nieuwe methoden (Collection)
        final String[] array2 = list.toArray(String[]::new);
        System.out.println(Arrays.toString(array2));
    }
}
